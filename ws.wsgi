#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import logging

activate_this = '/opt/py3/bin/activate_this.py'
with open(activate_this) as f:
    code = compile(f.read(), activate_this, 'exec')
    exec(code, dict(__file__=activate_this))

ENV_DIR= '/opt/py3'
#APP_DIR = '/var/www/wsgi/ws'
APP_DIR = '/home/ricardo/www/ws'

#activate_this = os.path.join(ENV_DIR, 'bin', 'activate_this.py')
#exec(open(activate_this, dict(__file__=activate_this)).read())
sys.path.append(ENV_DIR)

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,APP_DIR)

from ws import app as application
