#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)

app.config.from_pyfile('config.py')

from views.ws import *

if __name__ == '__main__':
    app.run()
