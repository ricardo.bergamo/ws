#!/usr/bin/env python

from flask import render_template
from ws import *

@app.route('/')
def index():
    return render_template('ws.html',
                               title='Memento Mori')

@app.route('/baudelaire')
def baud():
    return render_template('baud.html',
                               title='Baudelaire')

@app.route('/poe')
def poe():
    return render_template('poe.html',
                               title='Edgar Allan Poe')

@app.route('/leminski')
def lemi():
    return render_template('lemi.html',
                               title='Leminski')

@app.route('/shakespeare')
def shak():
    return render_template('shak.html',
                               title='Shakespeare')
